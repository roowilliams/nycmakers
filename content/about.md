+++
date = "2016-02-14T16:11:58+05:30"
draft = false
title = "NYC Makers"

+++

...is a project that aims to connect makers living and working in NYC. Currently we are building this out using Slack, but plan physical events

# Rationale

NYC can feel like a pretty lonely place until you build up a network of people around you who you like, respect and gel with. NYC is a transient melting pot of a city and many people are here because they want to DO/MAKE something.

I've met a few makers and wanted to bring them together into a community... selfishly because I want to surround myself with peers full of positive energy that are doing interesting things, but also because I think there's a space for the makers of NYC to come together online.


If you aren't familiar with Slack, it's basically a chat room that you can hang out in. It can be accessed through a browser, installed on your computer as a desktop app, or installed on your phone. On my computer, I have mine launch on startup so even if I'm not actively chatting then I'm always available and the community is within reach.

The benefits are obvious - from material/resource sharing, to project feedback, motivation, job opportunities, education and just good, friendly banter.

The community is set up to allow members to invite fellow makers and grow this community.

# To Do

- Write introduction and set up bot to autogreet
- Allow signups from the web using: https://github.com/rauchg/slackin
- Or this: https://levels.io/slack-typeform-auto-invite-sign-ups/
- Publish to http://www.slacklist.info


# Future Ideas

Meetups: organise meetups; show and tell, workshops, networking. We can use Anomaly's event space for this.
Makers camp: we head off into the woods armed with camping equipment, materials and tools. We spend a weekend making installations in the woods, document it, then leave without trace.