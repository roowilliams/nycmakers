+++
date = "2016-10-13T11:31:58+05:00"
draft = false
title = "Start"

+++

We're up to 8 members, a mix of designers, creative technologists, UX bods and developers. If I were to use a metaphor, then I'd say that we're currently awkward guests at a party where no one knows each other, spending the night looking at our phones. And we're also busy!

Currently thinking on ideas for an interesting way for people to introduce themselves, spark conversation and create connections.